GameSettingControls.Monsoon = class extends GameSettingControlCheckbox
{
	constructor(...args)
	{
		super(...args);
		g_GameSettings.monsoon.watch(this.render.bind(this), ["available", "enabled"]);
		this.render();
	}

	render()
	{
		this.setHidden(!g_GameSettings.monsoon.available);
		this.setChecked(g_GameSettings.monsoon.enabled);
	}

	onPress(checked)
	{
		g_GameSettings.monsoon.enabled = checked;
		this.gameSettingsController.setNetworkInitAttributes();
	}
};

GameSettingControls.Monsoon.prototype.TitleCaption = translate("Monsoon");
