class Monsoon
{
	static Schema = "<a:component type='system'/><empty/>";
	static waterLevelDelta = 0.496;
	static drySeasonDuration = 130000;
	static monsoonSeasonDuration = 43200;
	static deltaDelay = 2400;
	static weatherWarningDuration = 14400;
	static weatherWarning = "The monsoon has started. Soon the lakes will swallow the land. You " +
		"should evacuate the units.";
	static monsoonEndMessage = "The water will drain away soon, revealing all the new resources.";

	Start(waterLevelRange)
	{
		this.waterLevelRange = waterLevelRange;
		this.fertileResources = Monsoon.GetFertileResources(waterLevelRange.max);
		this.timer = Engine.QueryInterface(SYSTEM_ENTITY, IID_Timer)
		this.timer.SetTimeout(SYSTEM_ENTITY, IID_Monsoon, "DisplayWeatherWarning",
			Monsoon.drySeasonDuration, {"currentWaterLevel" : waterLevelRange.min});
	}

	static GetFertileResources(maxWaterLevel)
	{
		const templateManager = Engine.QueryInterface(SYSTEM_ENTITY, IID_TemplateManager)
		return Engine.QueryInterface(SYSTEM_ENTITY, IID_RangeManager).GetEntitiesByPlayer(0).filter(
		(ent) => {
			const cmpPosition = Engine.QueryInterface(ent, IID_Position);
			if (!cmpPosition?.IsInWorld())
				return false;

			if (cmpPosition.GetPosition().y > maxWaterLevel)
				return false;

			return Engine.QueryInterface(ent, IID_ResourceSupply);
		}).map((ent) =>
		({
			"number" : ent,
			"position" : Engine.QueryInterface(ent, IID_Position).GetPosition(),
			"template" : templateManager.GetCurrentTemplateName(ent)
		}));
	}

	static DestroySuncUnits(currentWaterLevel)
	{
		for (const ent of Engine.QueryInterface(SYSTEM_ENTITY, IID_RangeManager).GetNonGaiaEntities())
		{
			const cmpPosition = Engine.QueryInterface(ent, IID_Position);
			if (!cmpPosition?.IsInWorld())
				continue;

			if (cmpPosition.GetPosition().y >= currentWaterLevel)
				continue;

			if (Engine.QueryInterface(ent, IID_Identity)?.HasClass("Field"))
				continue;

			Engine.QueryInterface(ent, IID_Health)?.Kill();
		}
	}

	static DestroyStrandedShips(currentWaterLevel)
	{
		for (const ent of Engine.QueryInterface(SYSTEM_ENTITY, IID_RangeManager).GetNonGaiaEntities())
		{
			const cmpPosition = Engine.QueryInterface(ent, IID_Position);
			if (!cmpPosition?.IsInWorld())
				continue;

			if (cmpPosition.GetPosition().y == currentWaterLevel)
				continue;

			if (Engine.QueryInterface(ent, IID_Identity)?.HasClass("Ship"))
				Engine.QueryInterface(ent, IID_Health).Kill();
		}
	}

	static GrowResources(fertileResources)
	{
		for (const entityInfo of fertileResources)
		{
			const resource = Engine.QueryInterface(entityInfo.number, IID_ResourceSupply);
			if (resource)
				resource.SetAmount(resource.GetMaxAmount());
			else
			{
				entityInfo.number = Engine.AddEntity(entityInfo.template);
				Engine.QueryInterface(entityInfo.number, IID_Ownership).SetOwner(0);

				Engine.QueryInterface(entityInfo.number, IID_Position).JumpTo(entityInfo.position.x,
					entityInfo.position.z);
			}
		}
	}

	// Each of the following function will invoke the next. The last will invoke the first. Forming an
	// endless loop.

	DisplayWeatherWarning(state)
	{
		Engine.QueryInterface(SYSTEM_ENTITY, IID_GuiInterface).AddTimeNotification({
			"message": Monsoon.weatherWarning
		});

		this.timer.SetTimeout(SYSTEM_ENTITY, IID_Monsoon, "IncreaseWaterLevel",
			Monsoon.weatherWarningDuration, state);
	}

	IncreaseWaterLevel(state)
	{
		if (state.currentWaterLevel < this.waterLevelRange.max)
		{
			state.currentWaterLevel += Monsoon.waterLevelDelta;
			Engine.QueryInterface(SYSTEM_ENTITY, IID_WaterManager).SetWaterLevel(
				state.currentWaterLevel);
			Monsoon.DestroySuncUnits(state.currentWaterLevel);
			this.timer.SetTimeout(SYSTEM_ENTITY, IID_Monsoon, "IncreaseWaterLevel",
				Monsoon.deltaDelay, state);
		}
		else
			this.timer.SetTimeout(SYSTEM_ENTITY, IID_Monsoon, "ApplyFertility",
				Monsoon.monsoonSeasonDuration, state);
	}

	ApplyFertility(state)
	{
		Monsoon.GrowResources(this.fertileResources);

		Engine.QueryInterface(SYSTEM_ENTITY, IID_GuiInterface).AddTimeNotification({
			"message": Monsoon.monsoonEndMessage
		});

		this.timer.SetTimeout(SYSTEM_ENTITY, IID_Monsoon, "DecreaseWaterLevel", Monsoon.deltaDelay,
			state);
	}

	DecreaseWaterLevel(state)
	{
		if (state.currentWaterLevel > this.waterLevelRange.min)
		{
			state.currentWaterLevel -= Monsoon.waterLevelDelta;
			Engine.QueryInterface(SYSTEM_ENTITY, IID_WaterManager).SetWaterLevel(
				state.currentWaterLevel);
			Monsoon.DestroyStrandedShips(state.currentWaterLevel);
			this.timer.SetTimeout(SYSTEM_ENTITY, IID_Monsoon, "DecreaseWaterLevel",
				Monsoon.deltaDelay, state);
		}
		else
			this.timer.SetTimeout(SYSTEM_ENTITY, IID_Monsoon, "DisplayWeatherWarning",
				Monsoon.drySeasonDuration, state);
	}
}

Engine.RegisterSystemComponentType(IID_Monsoon, "Monsoon", Monsoon);
