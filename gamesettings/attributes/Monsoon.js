GameSettings.prototype.Attributes.Monsoon = class extends GameSetting
{
	init()
	{
		this.available = !!this.getWaterLevelRangeForCurrentMap();
		this.enabled = false;
		this.settings.map.watch(this.onMapChange.bind(this), ["map"]);
	}

	toInitAttributes(attribs)
	{
		if (this.enabled)
			attribs.settings.monsoon = {"waterLevelRange" : this.getWaterLevelRangeForCurrentMap()};
	}

	fromInitAttributes(attribs)
	{
		this.enabled = !!this.getLegacySetting(attribs, "Monsoon");
	}

	onMapChange()
	{
		this.available = !!this.getWaterLevelRangeForCurrentMap();
	}

	getWaterLevelRangeForCurrentMap()
	{
		return this.getMapSetting("WaterLevelRange") ??
			GameSettings.prototype.Attributes.Monsoon.supportedMaps.get(this.getMapSetting("Name"));
	}

	// I don't want to copy 1MB maps in to Epsilono so i hard code the supported maps here alongside the
	// WaterLevelRange. If you wan't an example how it's done right see cantabrian_highlands.json.
	static supportedMaps = new Map([
		["Acropolis Bay (2)", {"min" : 20, "max" : 36}],
		["Death Canyon (2)", {"min" : 20, "max" : 50}],
		["Deccan Plateau (2 players)", {"min" : 20, "max" : 36}],
		["Greek Acropolis (2)", {"min" : 20, "max" : 36}],
		["Greek Acropolis Night(2)", {"min" : 20, "max" : 36}],
		["Miletus Peninsula (2)", {"min" : 20, "max" : 36}],
		["Tuscan Acropolis (4)", {"min" : 18, "max" : 40}]
	]);
}
